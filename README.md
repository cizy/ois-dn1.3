# Pregovori

### Sivi lasje so Božji grafiti.
Bill Cosby

### Računalnik me je enkrat premagal v šahu, toda v kick boxingu pa mi ni bil kos.
Neznan avtor

### Moj nasvet je, da se poročiš: če najdeš dobro ženo, boš srečen; če ne, boš pač filozof.
Sokrat

### Ko sem bil star štirinajst let, se mi je oče zdel tako neumen, da sem ga komaj prenašal. Ko sem bil star enaindvajset let, pa sem bil presenečen, koliko se je ta stari mož v sedmih letih naučil.
Mark Twain

### Beseda ni konj.
Neznan avtor